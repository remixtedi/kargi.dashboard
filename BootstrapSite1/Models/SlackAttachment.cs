﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace kargiDashboard.Models
{
    public class Field
    {
        public string title { get; set; }
        public string value { get; set; }
        public bool @short { get; set; }
    }

    public class Attachment
    {
        public string color { get; set; }
        public string title { get; set; }
        public string title_link { get; set; }
        public string text { get; set; }
        public List<Field> fields = new List<Field>();
        public string footer { get; set; }
    }
}