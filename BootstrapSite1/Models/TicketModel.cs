﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace kargiDashboard.Models
{
    public class TicketModel
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Summary { get; set; }
        [Required]
        public string Description { get; set; }
        public string Status { get; set; }
        public string UserPhone { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateDateLocal => CreateDate.ToLocalTime().ToString("MM-dd-yyyy hh:mm:ss tt");
        public string CreateDateShortDate => CreateDate.ToLocalTime().ToShortDateString();
        public string CreateDateShortTime => CreateDate.ToLocalTime().ToShortTimeString();
        public DateTime ResolveDate { get; set; }
        public string ResolveDateLocal => getString(ResolveDate.ToLocalTime());
        public DateTime CloseDate { get; set; }
        public string CloseDateLocal => getString(CloseDate.ToLocalTime());
        public string Resolver { get; set; }
        public string Closer { get; set; }
        public bool SlackAlert { get; set; }
        public string ResolveComment { get; set; }
        [Required]
        public string JiraId { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("Entries")]
        public IEnumerable<EntryModel> Entries { get; set; }

        public string getString(DateTime date)
        {
            if (date.ToString() == "1/1/0001 12:00:00 AM" || date.ToString() == "1/1/0001 4:00:00 AM")
                return string.Empty;
            else
                return date.ToString();
        }
    }
    public class EntryModel
    {
        public string Author { get; set; }
        public string Comment { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateDateLocal => CreateDate.ToLocalTime().ToString("MM-dd-yyyy hh:mm:ss tt");
        public string FromStatus { get; set; }
        public string ToStatus { get; set; }
    }



}