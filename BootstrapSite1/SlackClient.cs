﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace kargiDashboard
{
    public class SlackClient
    {
       


        private readonly Uri _uri;
        private readonly Encoding _encoding = new UTF8Encoding();

        public SlackClient(string urlWithAccessToken)
        {
            _uri = new Uri(urlWithAccessToken);
        }


        //Post a message using simple strings  
        public void PostMessage(Array Attachments, string channel = null)
        {
            Payload payload = new Payload();
            {
                payload = new Payload()
                {
                    link_names = "1",
                    Channel = channel,
                    Username = "Support",
                    Text = null,
                    Attachments = Attachments
                };
            }
            PostMessage(payload);
        }
        //Post Alerts from browser check
        public void PostMessageBrowser(string text, string username = null, string channel = null)
        {
            Payloadbrowser payloadbrowser = new Payloadbrowser()
            {
                link_names = "1",
                Channel = channel,
                Username = username,
                Text = text
            };

            PostMessageBrowser(payloadbrowser);
        }

        //Post a message using a Payload object  
        public void PostMessage(Payload payload)
        {
            string payloadJson = JsonConvert.SerializeObject(payload);
                
            
            using (WebClient client = new WebClient())
            {
                NameValueCollection data = new NameValueCollection();
                data["payload"] = payloadJson;

                var response = client.UploadValues(_uri, "POST", data);

                //The response text is usually "ok"  
                string responseText = _encoding.GetString(response);
            }
        }

        public void PostMessageBrowser(Payloadbrowser payloadbrowser)
        {
            string payloadbrowserJson = JsonConvert.SerializeObject(payloadbrowser);

            using (WebClient client = new WebClient())
            {
                NameValueCollection data = new NameValueCollection();
                data["payload"] = payloadbrowserJson;

                var response = client.UploadValues(_uri, "POST", data);

                //The response text is usually "ok"  
                string responseText = _encoding.GetString(response);
            }
        }
    }

    //This class serializes into the Json payload required by Slack Incoming WebHooks  
    public class Payload
    {
        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("link_names")]
        public string link_names { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("attachments")]
        public Array Attachments { get; set; }

    }

    public class Payloadbrowser
    {
        [JsonProperty("channel")]
        public string Channel { get; set; }

        [JsonProperty("link_names")]
        public string link_names { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

    }


}
