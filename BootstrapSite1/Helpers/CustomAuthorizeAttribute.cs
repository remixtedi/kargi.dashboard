﻿using MongoDB.Driver;
using kargiDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace kargiDashboard.Helpers
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.User.Identity.IsAuthenticated)
            {
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString;
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = $"SELECT * FROM adminusers WHERE username='{httpContext.User.Identity.Name.ToLower()}'";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                reader = cmd.ExecuteReader();

                

                if (reader.HasRows && reader.Read())
                {
                    if (reader.GetString(reader.GetOrdinal("roles")).Any(s => Roles.Contains(s)))
                    {
                        conn.Close();
                        return true;
                    }
                    else
                    {
                        conn.Close();
                        return false;
                    }
                }
                else
                {
                    conn.Close();
                    return base.AuthorizeCore(httpContext);
                }
            }
            else
            {
                return base.AuthorizeCore(httpContext);
            }
        }
    }
}