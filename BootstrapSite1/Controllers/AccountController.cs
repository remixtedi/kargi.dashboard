﻿using kargiDashboard.Models;
using MongoDB.Driver;
using kargiDashboard.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Collections;
using System.Security.Cryptography;

namespace kargiDashboard.Controllers
{
    public class AccountController : Controller
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;


        // GET: Account
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }



        

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            // TODO: get user by model.Username
            UserModel User = GetUserPassword(model.Username);
            
            var succes = Helpers.AuthorizationHelpers.VerifyHashedPassword(User.password, model.Password);

            if (succes)
            {
                FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);

                    return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Incorrect Username or Password.");
                return View();
            }

        }
        UserModel GetUserPassword(string Username)
        {
            UserModel User = new UserModel();
            User.username = Username;
            try
            {
                String connString = System.Configuration.ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString;
                conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
                conn.Open();
                queryStr = $"SELECT * FROM adminusers WHERE username='{Username}'";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                reader = cmd.ExecuteReader();

                if (reader.HasRows && reader.Read())
                {
                    string roles = string.Empty;
                    User.password = reader.GetString(reader.GetOrdinal("password"));
                    User.fullname = reader.GetString(reader.GetOrdinal("fullname"));
                    User.roles = reader.GetString(reader.GetOrdinal("roles")).Split(',').ToArray<string>();
                }

                conn.Close();

                if (User != null)
                    return User;
                else
                    return new UserModel();
            }
            catch {
                return new UserModel();
            }
        }

        string GetUsername(string Username)
        {
            try
            {
                IMongoClient Client = HomeController.GetMongoClient();
                var database = Client.GetDatabase("Production");
                var _collection = database.GetCollection<UserModel>("Users");

                UserModel User = _collection.Find(Builders<UserModel>.Filter.Eq("Username", Username.ToLower())).FirstOrDefault();

                if (User != null)
                    return User.password;
                else
                    return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}