﻿using MongoDB.Driver;
using kargiDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace kargiDashboard.Controllers
{
    public class MenuController : Controller
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;

        // GET: Menu
        public ActionResult ShowMenu()
        {
            UserModel User = new UserModel();

            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString;
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);
            conn.Open();
            queryStr = $"SELECT * FROM adminusers WHERE username='{this.HttpContext.User.Identity.Name.ToLower()}'";

            cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
            reader = cmd.ExecuteReader();

            if (reader.HasRows && reader.Read())
            {
                string roles = string.Empty;
                User.username = reader.GetString(reader.GetOrdinal("username"));
                User.password = reader.GetString(reader.GetOrdinal("password"));
                User.fullname = reader.GetString(reader.GetOrdinal("fullname"));
                User.roles = reader.GetString(reader.GetOrdinal("roles")).Split(',').ToArray<string>();
            }


            conn.Close();

            return PartialView("Menu", User);
        }

        public ActionResult ShowHeader()
        {
            return PartialView("Header");
        }
    }
}