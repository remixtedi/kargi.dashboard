﻿using kargiDashboard.Helpers;
using kargiDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;

namespace kargiDashboard.Controllers
{
    [CustomAuthorize(Roles = "admin")]
    public class UsersController : Controller
    {
        MySql.Data.MySqlClient.MySqlConnection conn;
        MySql.Data.MySqlClient.MySqlCommand cmd;
        MySql.Data.MySqlClient.MySqlDataReader reader;
        String queryStr;

        public UsersController()
        {
            String connString = System.Configuration.ConfigurationManager.ConnectionStrings["MySqlConnection"].ConnectionString;
            conn = new MySql.Data.MySqlClient.MySqlConnection(connString);

        }

        // GET: Users
        public ActionResult Index()
        {
            if (TempData.ContainsKey("AlreadyRegistered"))
            {
                ViewBag.ErrorMessage = TempData["AlreadyRegistered"].ToString();
            }

            return View();
        }

        public ActionResult ShowUsers(string Id)
        {
            List<UserModel> Users = new List<UserModel>();
            conn.Open();
            queryStr = $"SELECT * FROM adminusers";
            cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);

            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    Users.Add(new UserModel {
                        userId = Convert.ToInt32(reader.GetString(reader.GetOrdinal("userId"))),
                        fullname = reader.GetString(reader.GetOrdinal("fullname")),
                        username = reader.GetString(reader.GetOrdinal("username")),
                        password = reader.GetString(reader.GetOrdinal("password")),
                        roles = reader.GetString(reader.GetOrdinal("roles")).Split(',').AsEnumerable(),

                    });
                }
            }

            conn.Close();

            return PartialView("ShowUsers", Users);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUser(FormCollection form)
        {
            if (form.ToValueProvider() != null)
            {

                conn.Open();
                queryStr = $"SELECT * FROM adminusers WHERE username = '{form["username"]}'";
                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                reader = cmd.ExecuteReader();

                if (reader.HasRows && reader.Read())
                {
                    TempData["AlreadyRegistered"] = "Username Already Registered!";
                    return RedirectToAction("Index", "Users");
                }
                conn.Close();
                conn.Open();


                queryStr = $"INSERT INTO adminusers (fullname, username, password, roles)VALUES('{form["fullname"]}','{form["username"]}','{HashPassword(form["password"])}','{form["roles"]}')";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                reader = cmd.ExecuteReader();
                conn.Close();

                return RedirectToAction("Index", "Users");
            }

            return RedirectToAction("Index", "Users");
        }

        public ActionResult DeleteUser(int id)
        {
            if (id > 0)
            {
                conn.Open();
                queryStr = $"DELETE FROM adminusers WHERE userId = {id}";

                cmd = new MySql.Data.MySqlClient.MySqlCommand(queryStr, conn);
                reader = cmd.ExecuteReader();
                conn.Close();
            }

            // Show Index view
            return RedirectToAction("Index");
        }

        public static string HashPassword(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
        }
    }
}